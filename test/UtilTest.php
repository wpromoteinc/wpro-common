<?php namespace Wpro\Common\Test;

use Wpro\Common\Util;

class UtilTest extends \PHPUnit_Framework_TestCase {

    /**
     * @test
     */
    public function it_converts_string_to_camelcase() {
        $string1 = 'foo_bar';
        $string2 = 'foo_bar_fizz_buzz';
        $string3 = 'foo_bar_Hello_World';
        $string4 = 'foo_bar_AwesomeHome';

        static::assertEquals('fooBar', Util::toCamelCase($string1));
        static::assertEquals('fooBarFizzBuzz', Util::toCamelCase($string2));
        static::assertEquals('fooBarHelloWorld', Util::toCamelCase($string3));
        static::assertEquals('fooBarAwesomeHome', Util::toCamelCase($string4));
    }

    /**
     * @test
     */
    public function it_converts_string_to_underscore() {
        $string1 = 'fooBar';
        $string2 = 'fooBarAwesome';
        $string3 = 'FooBarAwesome_test';
        $string4 = 'foo_Bar_testing';

        static::assertEquals('foo_bar', Util::toUnderscore($string1));
        static::assertEquals('foo_bar_awesome', Util::toUnderscore($string2));
        static::assertEquals('foo_bar_awesome_test', Util::toUnderscore($string3));
        static::assertEquals('foo__bar_testing', Util::toUnderscore($string4));
    }
}