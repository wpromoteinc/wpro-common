<?php namespace Wpro\Common\Test\Traits;

use Wpro\Common\Test\Traits\Mocks\GetterMock;
use Wpro\Common\Test\Traits\Mocks\SetterMock;

class GetterSetterTest extends \PHPUnit_Framework_TestCase {

    /**
     * @test
     */
    public function it_gets_properties_with_magic_methods() {
        $getterMock = new GetterMock();

        static::assertEquals('test_name', $getterMock->name);
        static::assertEquals('test_fizz', $getterMock->fizz);
        static::assertEquals('test_foobar', $getterMock->fooBar);
        static::assertEquals('test_foobar', $getterMock->foo_bar);
    }

    /**
     * @test
     */
    public function it_sets_properties_with_magic_methods() {
        $setterMock = new SetterMock();

        $setterMock->fooBar = 'foobar test 1';
        static::assertEquals('FOOBAR TEST 1', $setterMock->getFooBar());

        $setterMock->foo_bar = 'foobar test 2';
        static::assertEquals('FOOBAR TEST 2', $setterMock->getFooBar());

        $setterMock->name = 'name test';
        static::assertEquals('name test', $setterMock->getName());

        $setterMock->fizz = 'fizz test';
        static::assertEquals('fizz test', $setterMock->getFizz());
    }
}