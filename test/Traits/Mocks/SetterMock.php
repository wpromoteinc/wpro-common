<?php namespace Wpro\Common\Test\Traits\Mocks;

use Wpro\Common\Traits\Setter;

class SetterMock {

    use Setter;

    private $fizz;
    private $fooBar;

    private $__name;

    public function setFooBar($foobar) {
        $this->fooBar = strtoupper($foobar);
    }

    public function setName($name) {
        $this->__name = $name;
    }

    public function getName() {
        return $this->__name;
    }

    public function getFizz() {
        return $this->fizz;
    }

    public function getFooBar() {
        return $this->fooBar;
    }
}