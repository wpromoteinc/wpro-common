<?php namespace Wpro\Common\Test\Traits\Mocks;

use Wpro\Common\Traits\Getter;

/**
 * @property string $fizz
 * @property string $foobar
 * @property string $name
 */
class GetterMock {

    use Getter;

    private $fizz   = 'test_fizz';
    private $fooBar = 'test_foobar';

    public function getName() {
        return 'test_name';
    }
}