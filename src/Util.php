<?php namespace Wpro\Common;

class Util {

    /**
     * @param string $string
     * @return string
     */
    public static function toCamelCase($string) {
        return lcfirst(strtr(ucwords(strtr($string, ['_' => ' '])), [' ' => '']));
    }

    /**
     * @param string $string
     * @return string
     */
    public static function toUnderscore($string) {
        $underscoreName = '';
        $len            = strlen($string);

        for ($i = 0; $i < $len; $i++) {
            if (0 !== $i && ctype_upper($string[ $i ])) {
                $underscoreName .= '_' . $string[ $i ];
            } else {
                $underscoreName .= $string[ $i ];
            }
        }

        return strtolower($underscoreName);
    }
}