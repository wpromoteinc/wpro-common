<?php namespace Wpro\Common\Contracts;

interface ArrayableInterface {

    /**
     * @return array
     */
    public function toArray();
}