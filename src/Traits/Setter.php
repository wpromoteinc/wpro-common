<?php namespace Wpro\Common\Traits;

use Wpro\Common\Util;

trait Setter {

    public function __set($name, $value) {
        try {
            $this->writeProperty(Util::toCamelCase($name), $value);
        } catch (\InvalidArgumentException $ex) {
            $this->writeProperty($name, $value);
        }
    }

    private function writeProperty($name, $value) {

        if (method_exists($this, 'set' . $name)) {
            $this->{'set'.$name}($value);
            return;
        } elseif (property_exists($this, $name)) {
            $this->{$name} = $value;
            return;
        }

        throw new \InvalidArgumentException(
            sprintf('The property "%s" does not exist on "%s".', $name, get_class($this))
        );
    }
}