<?php namespace Wpro\Common\Traits;

trait GetterSetter {

    use Getter, Setter;
}