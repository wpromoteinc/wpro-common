<?php namespace Wpro\Common\Traits;

use Wpro\Common\Util;

trait Getter {

    public function __get($name) {
        try {
            return $this->readProperty(Util::toCamelCase($name));
        } catch (\InvalidArgumentException $ex) {
            return $this->readProperty($name);
        }
    }

    private function readProperty($name) {
        if (method_exists($this, $name)) {
            return $this->{$name}();
        }
        if (method_exists($this, 'get' . $name)) {
            return $this->{'get'.$name}();
        }
        if (property_exists($this, $name)) {
            return $this->{$name};
        }

        throw new \InvalidArgumentException(
            sprintf('The property "%s" does not exist on "%s".', $name, get_class($this))
        );
    }
}